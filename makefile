all: basic

RMG_PATH = out/production/

RMG_BRO_RUN = rmg/bro/main/RMGRunner
RMG_ANALYZE_RUN = rmg/analyzer/RMGAnalyzer

basic: $(RMG_PATH)/$(RMG_BRO_RUN).class
	cd $(RMG_PATH); java $(RMG_BRO_RUN) -f

subset_rel1: $(RMG_PATH)/$(RMG_ANALYZE_RUN).class
	cd $(RMG_PATH); java $(RMG_ANALYZE_RUN) -q -f

subset_rel2: $(RMG_PATH)/$(RMG_ANALYZE_RUN).class
	cd $(RMG_PATH); time java $(RMG_ANALYZE_RUN) -k 3,5,26,26 -r 3,5,26,26 -f -q -d $(CURDIR)/seeds/ -c 10;
#	rm -f $@.out
#	cd $(RMG_PATH) ; \
#		cat ../../subset_rel2.test | while read par ; do \
#			echo $$par | cut -d' ' -f1 | tr -d "\n" >> ../../$@.out ; \
#			printf "   " >> ../../$@.out ; \
#			echo $$par | cut -d' ' -f2- | xargs java $(RMG_ANALYZE_RUN) | tr "\n" " " >> ../../$@.out ; \
#			printf "\n" >> ../../$@.out ; \
#		done

subset_rel3: $(RMG_PATH)/$(RMG_ANALYZE_RUN).class
	cd $(RMG_PATH); time java $(RMG_ANALYZE_RUN) -knownRelations -- -k 1,5,15,15 -r 1,5,15,15 -f -q -c 33000;

subset_rel4: $(RMG_PATH)/$(RMG_ANALYZE_RUN).class
	cd $(RMG_PATH); time java $(RMG_ANALYZE_RUN) -q -f -c 100; time java $(RMG_ANALYZE_RUN) -q -f -c 1000; time java $(RMG_ANALYZE_RUN) -q -f -c 10000;

help: $(RMG_PATH)/$(RMG_BRO_RUN).class
	cd $(RMG_PATH); java $(RMG_BRO_RUN) -h

clean:
	rm -rf seeds *.dat