package rmg.analyzer;

import rmg.bro.dto.Interpretation;
import rmg.bro.dto.RevisionedModels;
import rmg.bro.operators.*;
import rmg.bro.main.RMGRunner;

import java.util.*;

public class RMGAnalyzer {

    private static final BeliefRevOp d = new Dalal(), s = new Satoh(), w = new Winslett(), f = new Forbus();
    private static final String dalalSatoh = "-dalalSatoh";
    private static final String forbusWinslett = "-forbusWinslett";
    private static final String knownRel = "-knownRelations";

    public static void main(String[] args) {
/*
        String[] newArgs = new String[args.length + 1];
        newArgs[0] = "-q";
        System.arraycopy(args, 0, newArgs, 1, args.length);
*/

//        RMGRunner.main(newArgs);

//        Map<Integer, Set<RevisionedModels>> models = getEvaluations();
        String[] rmgArgs = null, analyzerArgs = null;
        for(int i=0;i<args.length;i++) {
            if(args[i].equals("--")) {
                rmgArgs = new String[args.length-i-1];
                System.arraycopy(args,i+1,rmgArgs,0,rmgArgs.length);

                analyzerArgs = new String[i];
                System.arraycopy(args,0,analyzerArgs,0,analyzerArgs.length);
                break;
            }
        }

        if(analyzerArgs == null) {
            analyzerArgs = args;
            rmgArgs = new String[0];
        }

        if(analyzerArgs.length == 0) {
            System.err.println("Specify analysis.");
            printHelp();
            return;
        }

        RMGRunner.main(rmgArgs);
        Map<Integer,Set<RevisionedModels>> models = RMGRunner.getEvaluations();

        Map<String, Set<Interpretation>> images = buildImages(models);

        Iterator<String> it = Arrays.stream(analyzerArgs).iterator();

        while (it.hasNext()) {
            switch (it.next()) {
                case knownRel -> checkKnownSubsetRelations(images);
                case forbusWinslett -> checkForbusAndWinslett(images);
                case dalalSatoh -> checkDalalAndSatoh(images);
                default -> {
                    System.err.println("Unknown option.");
                    printHelp();
                    return;
                }
            }
        }

//        System.out.println("Total number of merged models: " + models.size());

    }

    private static void printHelp() {
        System.out.println("Run the program as follows:\n\n\t"
                            + "java RMGAnalyzer <Analysis1> [<Analysis2> ...] [-- <RMGRunnerOption1> [RMGRunnerOption2 ...]]\n\n"
                            + "The double dash '--' has to be specified as a separator between analyzer and runner arguments."
                            + "Available analyer options are:\n\n\t"
                            + knownRel+"\t\tCheck the mathematically proven relations based on generated data.\n\t"
                            + forbusWinslett+"\t\tCheck the relation between the images of Forbus and Winslett operators.\n\t"
                            + dalalSatoh+"\t\tCheck the relation between the images of Dalal and Satoh operators.");
    }

    public static void checkForbusAndWinslett(Map<String,Set<Interpretation>> images) {
        Set<Interpretation> forbus = images.get(f.getName()),
                winslett = images.get(w.getName());

        System.out.println("forbus sub winslet: " + winslett.containsAll(forbus));
        System.out.println("winslett sub forbus: " + forbus.containsAll(winslett));
    }

    public static void checkDalalAndSatoh(Map<String,Set<Interpretation>> images) {
        Set<Interpretation> dalal = images.get(d.getName()),
                satoh = images.get(s.getName());

        System.out.println("dalal sub satoh: " + satoh.containsAll(dalal));
        System.out.println("satoh sub dalal: " + dalal.containsAll(satoh));
    }

    public static void checkKnownSubsetRelations(Map<String,Set<Interpretation>> images) {
        Set<Interpretation> dalal = images.get(d.getName()),
                            satoh = images.get(s.getName()),
                            winslett = images.get(w.getName()),
                            forbus = images.get(f.getName());

        Set<Interpretation> unionWF = new HashSet<>();
        Set<Interpretation> unionDWF = new HashSet<>();
        Set<Interpretation> unionDS = new HashSet<>();
//                winslett.forEach(p -> unionWF.add(new Interpretation(p)));
        unionWF.addAll(winslett);
        unionWF.addAll(forbus);

        unionDWF.addAll(unionWF);
        unionDWF.addAll(dalal);

        unionDS.addAll(satoh);
        unionDS.addAll(dalal);

        System.out.println("dalal !sub unionWF: " + !unionWF.containsAll(dalal));
        System.out.println("satoh !sub unionDWF: " + !unionDWF.containsAll(satoh));
        System.out.println("forbus !sub unionDS: " + !unionDS.containsAll(forbus));
        System.out.println("winslett !sub unionDS: " + !unionDS.containsAll(winslett));

    }

    private static Map<String,Set<Interpretation>> buildImages(Map<Integer,Set<RevisionedModels>> models) {
        Map<String,Set<Interpretation>> result = new HashMap<>();
        Set<Interpretation> dalal = new HashSet<>(), forbus = new HashSet<>(), winslett = new HashSet<>(), satoh = new HashSet<>();

        for (Set<RevisionedModels> m : models.values()) {

            for (RevisionedModels mm : m) {
                if (mm.getOperator().equals(d.getName())) dalal.addAll(mm.getModels());
                if (mm.getOperator().equals(s.getName())) satoh.addAll(mm.getModels());
                if (mm.getOperator().equals(w.getName())) winslett.addAll(mm.getModels());
                if (mm.getOperator().equals(f.getName())) forbus.addAll(mm.getModels());
            }
        }
        result.put(d.getName(),dalal);
        result.put(s.getName(),satoh);
        result.put(w.getName(),winslett);
        result.put(f.getName(),forbus);

        return result;
    }
}